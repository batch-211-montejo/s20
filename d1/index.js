// console.log("Hello World")

// Repetition Control Structure (Loops)
	// Loops are one of the most important deature that programming must have
	// It lets us execute code repeatedly in a pre-set number of or maybe forever

// miniactivity

	function greeting () {
		console.log("Hi, batch 211!");
	}
	greeting()
	greeting()
	greeting()
	greeting()
	greeting()
	greeting()
	greeting()
	greeting()
	greeting()
	greeting()

	// or we can just use loops 
	let countNum = 10;

	while(countNum !==0) {
		console.log("This is printed inside the loop: " + countNum);
		greeting();
		countNum--;
	}

	// While Loop
	/*
		A while loop tales in a nexpression/condition
		If the condition is evaluates to tru. the statement inside tha code block will be executed
	*/

	/*
		Syntax:
			while(expression/condition) {
				statement/code block
				final expression ++/-- (iteration)
			}

			-expression/condition - this are the unit of code that is being evaluated in our loop
			-Loop will run while the condition/expression is true

			-statement/code block = code/instruction that will be executed several times

			-finalExpression - indicated hwo to advance the loop
	*/

	let count = 5;
	// while the value of count is not equal to 0
	while(count !== 0) {
		//the current value of count is printed out
		console.log("While: " + count);
		// decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
		count --;
	}

	// let num = 20;

	// while (num !==0) {
	// 	console.log("While: num  " + num)
	// 	num --;
	// }

	let digit = 5;
	while (digit !== 20) {
		console.log("While: digit " + digit)
		digit ++;
	}

	// let num1 = 1;
	// while (num===2) {
	// 	console.log("While: num1 " + num1);
	// 	num1--
	// }


// Do While Loop

/*
	-A do while loop works a lot like the while loop but unlike while loops, do-while loops guarantee that the code will be executed at least once

	Syntax:
		do {
			statement/code block
			finalExpression ++/--
		} while (expression/condition)
*/

/*
	How the Do While loops works:
	1. the statements in the "do" block execute once
	2. The message "Do While" + number will be printed out in the console 
	3. After executing once, the while statement will evaluate whether to run the next iteration of the loop based on the given expression/condition
	4. If the expression/condition is not true, another iteration of the loop will be executed and will be repeated until the condition is met
	5. Id the condition is true, the loop will stop
*/


	// let number = Number(prompt("Give me a number"))
	// do {
	// 	console.log("Do While: " + number)
	// 	number+= 1;
	// } while (number<10)



// For Loop
	// a for loop is more flexible than while and do-while loops

	/*
		it consist of 3 parts
		1. Initialization valur that will trcak the progression of the loop
		2. Expression/Condition that will be evaluated which will determine if the loop will run one more time
		3. FinalExpression indicated how to advance the opp
	*/
	/*
		Syntax:

		for (initialization; expression/condition; final expression){
			statement
		}

	*/

	for (let count = 0; count <=20; count ++) {
		console.log("For Loop " + count);
	}

// miniactivity

	for (let count = 0; count<=20; count ++) {
		if (count%2===0 && count!==0){
		console.log("Even " + count);
		} 
	}


	// string

	let myString = "Camille Doroteo";
	// characters in strings may be counted using the .length property
	// strings are special compared to other data types
	console.log(myString.length);

	// Accesing elements of a string
	// individual characters of a string may be accessed using its index number
	// the first character in a string corresponds to the number 0, text is 1...

	console.log(myString[2]);
	console.log(myString[0]);
	console.log(myString[8]);
	console.log(myString[14]);


	// 

	for (let x = 0; x < myString.length; x++) {
		console.log(myString[x]);
	}

	// miniactivity

	let myFullName = "Justin Montejo";
	for (let x = 0; x < myFullName.length; x++) {
		console.log(myFullName[x]);
	}

	// 
	let myName = "NehEmIAh";
	let myNewName ="";

	for (let i =0; i<myName.length; i++) {
		if (
			myName[i].toLowerCase()== "a" ||
			myName[i].toLowerCase()== "e" ||
			myName[i].toLowerCase()== "i" ||
			myName[i].toLowerCase()== "o" ||
			myName[i].toLowerCase()== "u" 
		) {
			// if the letter in the name is a vowel, it will print the 3
			console.log(3);
		}
		else {
			// print in the cosole all non-vowel characters in the name
			console.log(myName[i]);
			myNewName += myName[i];
		}
	}

console.log(myNewName);//Nhmh
// concatenate the characters based on the given condition

// continue and break statement
/*
	the continue statement aloows the code to go to the next iteration of the loop without finishing the execution of all statement in a code block

	-The break statement is used to terminate the current loop once a match has been found
*/


	for (let count = 0; count<=20; count++) {
		if (count%2 === 0){
			console.log("Even Number");
			// tells the console to continue to the next itertaio of the loop
			continue;
			// this ignores all the statement located after the continue statement
			console.log("Hello");
		}
		console.log("Continue and Break: " +count);
		if (count>10) {
			// tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
			// number values after 10 will no longer be printed
			break;
		}
	}

	let name = "alexandro";
	 for (let i=0; i<name.length; i++) {
	 	// the current letter is printed out based on its index
	 	console.log(name[i]);

	 	if (name[i].toLowerCase() === "a") {
	 		console.log("Continue to the next iteration");
	 		continue;
	 }

	 if (name[i].toLowerCase() == "d"){
	 	break;
	 }
	}